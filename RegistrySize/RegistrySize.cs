﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace RegistrySize
{
    public class RegistrySize
    {
        public string Description { get; private set; }
        
        public RegistryKey RegistryKey { get; private set; }

        public int NumberOfKeys { get; private set; }

        public int NumberOfValues { get; private set; }

        public int SizeOfValues { get; private set; }

        public override string ToString()
        {
            return this.Description + " (Keys: " + NumberOfKeys + " Values: " + NumberOfValues + " Size: " + SizeOfValues + ")";
        }

        public void Add(RegistrySize size)
        {
            NumberOfKeys += size.NumberOfKeys;
            NumberOfValues += size.NumberOfValues;
            SizeOfValues += size.SizeOfValues;

        }

        public RegistrySize(string description, RegistryKey key)
        {
            this.Description = description;
            this.RegistryKey = key;

            string[] subkeys = key.GetSubKeyNames();
            string[] subvalues = key.GetValueNames();

            this.NumberOfKeys = subkeys.Length;
            this.NumberOfValues = subvalues.Length;

            foreach (string subValue in subvalues)
            {
                object o = key.GetValue(subValue);
                RegistryValueKind kind = key.GetValueKind(subValue);

                this.SizeOfValues += GetSizeOfValue(o, kind);
            }


            foreach (string s in subkeys)
            {
                try
                {
                    this.Add(new RegistrySize(s,key.OpenSubKey(s)));
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                }
            }

        }

        private int GetSizeOfValue(object value, RegistryValueKind kind)
        {
            switch (kind)
            {
                case RegistryValueKind.Binary:
                    return  ((byte[])value).Length;
                    break;
                case RegistryValueKind.DWord:
                    return  4;
                    break;
                case RegistryValueKind.MultiString:
                    foreach (string s in (string[])value)
                    {
                        return  s.Length * 2;
                    }
                    break;
                case RegistryValueKind.QWord:
                    return  8;
                    break;
                case RegistryValueKind.ExpandString:
                case RegistryValueKind.String:
                    return  ((string)value).Length * 2;
                    break;
                case RegistryValueKind.Unknown:
                    if (value == null)
                    {
                        return  0;
                    }
                    else
                    {
                        return  0;
                    }
                    break;
                default:
                    return 0;
            }
            return 0;
        }
    }
}
