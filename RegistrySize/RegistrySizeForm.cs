﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;

namespace RegistrySize
{
    public partial class RegistrySizeForm : Form
    {
        public RegistrySizeForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.RegistryHiveCombo.Items.Add("ClassesRoot");
            this.RegistryHiveCombo.Items.Add("CurrentConfig");
            this.RegistryHiveCombo.Items.Add("CurrentUser");
            this.RegistryHiveCombo.Items.Add("DynData");
            this.RegistryHiveCombo.Items.Add("LocalMachine");
            this.RegistryHiveCombo.Items.Add("PerformanceData");
            this.RegistryHiveCombo.Items.Add("Users");

            this.RegistryHiveCombo.SelectedItem = "CurrentUser";

        }

        private RegistryKey GetSelectedKey()
        {
            switch (this.RegistryHiveCombo.SelectedItem.ToString())
            {
                case "ClassesRoot": return Registry.ClassesRoot;
                case "CurrentConfig": return Registry.CurrentConfig;
                case "CurrentUser": return Registry.CurrentUser;
                case "DynData": return Registry.DynData;
                case "LocalMachine": return Registry.LocalMachine;
                case "PerformanceData": return Registry.PerformanceData;
                case "Users": return Registry.Users;
                default: return null;
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;

            Process notePad = new Process();
            notePad.StartInfo.FileName = @"C:\Program Files\SysinternalsSuite\regjump.exe";
            notePad.StartInfo.Arguments = @"HKCU\" + node.Tag;
            notePad.Start();
        }



        private void RefreshNodes(string keyname, TreeNodeCollection collection, bool recurse)
        {
            this.RegistryTreeView.BeginUpdate();
            try
            {
                List<RegistrySize> sizeList = new List<RegistrySize>();

                RegistryKey rootkey = GetSelectedKey();
                String[] keynames = rootkey.OpenSubKey(keyname).GetSubKeyNames();

                foreach (string s in keynames)
                {
                    //string subkeyname = keyname + (keyname == string.Empty ? string.Empty : @"\") + s;
                    RegistryKey key = rootkey.OpenSubKey((keyname == string.Empty ? keyname : keyname + "\\") + s);
                    if (key != null)
                    {
                        sizeList.Add(new RegistrySize(s, key));
                    }
                }

                foreach (RegistrySize size in sizeList.OrderByDescending(p => p.SizeOfValues))
                {
                    TreeNode subnode = new TreeNode();
                    collection.Add(subnode);
                    subnode.Text = size.ToString();
                    subnode.Tag = size.RegistryKey.Name.Replace(rootkey.Name + "\\", "");

                    if (recurse)
                    {
                        RefreshNodes(size.RegistryKey.Name.Replace(rootkey.Name + "\\",""), subnode.Nodes, false);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.RegistryTreeView.EndUpdate();
            }
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode node = e.Node;
            node.Nodes.Clear();

            string keyname = node.Tag.ToString();
            TreeNodeCollection collection = node.Nodes;

            RefreshNodes(keyname, collection, true);
            //node.Expand();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            this.RegistryTreeView.Nodes.Clear();
            RefreshNodes("", this.RegistryTreeView.Nodes, true);
        }


    }
}
